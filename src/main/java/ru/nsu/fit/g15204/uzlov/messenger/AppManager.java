package ru.nsu.fit.g15204.uzlov.messenger;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.Connection;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.CredentialsSerializer;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.DialogueData;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.DialogueDataSerializer;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.MessageData;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.MessageDataSerializer;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.TopicManager;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class AppManager {
    private final Connection connection;
    private final List<Credentials> credentials = new ArrayList<>();
    private final TopicManager<Credentials> credentialsManager;
    private DialogueListManagerImpl dialogueListManager;
    private String name;

    public AppManager(Connection connection) {
        this.connection = connection;
        this.credentialsManager = connection.getTopicManager(CredentialsSerializer.class, ReservedTopic.CREDENTIALS_TOPIC.getName(), null);

        this.credentialsManager.poll(100000);
        var creds = this.credentialsManager.nextBatch();
        if (creds != null) {
            for (var r : creds) {
                this.credentials.add(r.value());
            }
        }
    }

    public void login(Consumer<Boolean> callback, Credentials credentials) {
        boolean found = false;
        for (var c : this.credentials) {
            if (c.getName().equals(credentials.getName()) &&
                c.getPassword().equals(credentials.getPassword())) {
                found = true;
                break;
            }
        }

        if (!found) {
            callback.accept(false);
            return;
        }

        this.name = credentials.getName();
        callback.accept(true);
    }

    public boolean register(Credentials credential) {
        for (var c : this.credentials) {
            if (c.getName().equals(credential.getName())) {
                return false;
            }
        }
        this.credentialsManager.send(credential);
        this.credentials.add(credential);

        return true;
    }

    public DialogueListManager getDialogueListManager() {
        if (this.name == null) {
            return null;
        }
        if (this.dialogueListManager == null) {
            this.dialogueListManager = new DialogueListManagerImpl();
        }
        return this.dialogueListManager;
    }

    private enum ReservedTopic {
        DIALOGUES_LIST_TOPIC("test"),
        CREDENTIALS_TOPIC("credentials");

        private final String name;

        ReservedTopic(String name) {
            this.name = name;
        }

        static boolean isReservedName(String name) {
            for (var t : ReservedTopic.values()) {
                if (t.getName().equals(name)) {
                    return true;
                }
            }

            return false;
        }

        public String getName() {
            return this.name;
        }
    }

    private static class MessageImpl implements Message {
        private final String author;
        private final String content;
        private final boolean fromSelf;

        private MessageImpl(String author, String content, boolean fromSelf) {
            this.author = author;
            this.content = content;
            this.fromSelf = fromSelf;
        }

        @Override
        public String getAuthorName() {
            return this.author;
        }

        @Override
        public String getContent() {
            return this.content;
        }

        @Override
        public boolean isFromSelf() {
            return this.fromSelf;
        }
    }

    private class DialogueListManagerImpl implements DialogueListManager {
        private TopicManager<DialogueData> topicManager;
        private List<Dialogue> dialogues = new ArrayList<>();
        private Consumer<List<Dialogue>> callback = null;

        public DialogueListManagerImpl() {
            this.topicManager = AppManager.this.connection.getTopicManager(DialogueDataSerializer.class, ReservedTopic.DIALOGUES_LIST_TOPIC.getName(), () -> {
                List<Dialogue> list = new ArrayList<>();
                while (true) {
                    var records = this.topicManager.nextBatch();
                    if (records == null) {
                        break;
                    }
                    this.processRecords(records, list);
                }

                if (list.isEmpty()) {
                    return;
                }

                this.dialogues.addAll(list);

                if (this.callback != null) {
                    this.callback.accept(list);
                }
            });
        }

        private void processRecords(ConsumerRecords<String, DialogueData> records, List<Dialogue> output) {
            if (records == null) {
                return;
            }

            for (var record : records) {
                var data = record.value();
                Dialogue dialogue = new DialogueImpl(data.getName());
                output.add(dialogue);
            }
        }

        @Override
        public List<Dialogue> getDialogues() {
            this.processRecords(this.topicManager.nextBatch(), this.dialogues);

            return this.dialogues;
        }

        @Override
        public void createDialogue(String name) throws DialogueCreationException {
            if (ReservedTopic.isReservedName(name)) {
                throw new DialogueCreationException("this name is reserved");
            }
            for (var d : this.dialogues) {
                if (d.getName().equals(name)) {
                    throw new DialogueCreationException("dialogue with this name already exists");
                }
            }
            DialogueData dialogueData = new DialogueData(name);
            this.topicManager.send(dialogueData);
        }

        @Override
        public void subscribeToUpdates(Consumer<List<Dialogue>> callback) {
            this.callback = callback;
        }
    }

    private class DialogueImpl implements Dialogue {
        private final String name;
        private TopicManager<MessageData> topicManager;
        private List<Message> messages = new ArrayList<>();
        private Consumer<List<Message>> callback = null;

        private DialogueImpl(String name) {
            this.name = name;
            this.topicManager = AppManager.this.connection.getTopicManager(MessageDataSerializer.class, name, () -> {
                List<Message> list = new ArrayList<>();
                while (true) {
                    var records = this.topicManager.nextBatch();
                    if (records == null) {
                        break;
                    }
                    this.processRecords(records, list);
                }

                if (list.isEmpty()) {
                    return;
                }

                this.messages.addAll(list);

                if (this.callback != null) {
                    this.callback.accept(list);
                }
            });
        }

        @Override
        public String getName() {
            return this.name;
        }

        @Override
        public String getLastMessage() {
            // TODO
            return "";
        }

        @Override
        public List<Message> getMessages() {
            this.processRecords(this.topicManager.nextBatch(), this.messages);

            return this.messages;
        }

        private void processRecords(ConsumerRecords<String, MessageData> records, List<Message> list) {
            if (records == null) {
                return;
            }

            for (var record : records) {
                var data = record.value();
                var name = data.getAuthorName();
                var content = data.getContent();
                boolean fromSelf = AppManager.this.name.equals(data.getAuthorName());
                Message message = new MessageImpl(name, content, fromSelf);
                list.add(message);
            }
        }

        @Override
        public void createMessage(String content) {
            MessageData messageData = new MessageData(AppManager.this.name, content);
            this.topicManager.send(messageData);
        }

        @Override
        public void subscribeToUpdates(Consumer<List<Message>> callback) {
            this.callback = callback;
        }
    }
}
