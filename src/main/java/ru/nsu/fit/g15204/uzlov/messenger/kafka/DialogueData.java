package ru.nsu.fit.g15204.uzlov.messenger.kafka;

public class DialogueData {
    private final String name;

    public DialogueData(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
