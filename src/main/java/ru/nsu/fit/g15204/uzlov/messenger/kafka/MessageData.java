package ru.nsu.fit.g15204.uzlov.messenger.kafka;

public class MessageData {
    private final String authorName;
    private final String content;

    public MessageData(String authorName, String content) {
        this.authorName = authorName;
        this.content = content;
    }

    public String getAuthorName() {
        return this.authorName;
    }

    public String getContent() {
        return this.content;
    }
}
