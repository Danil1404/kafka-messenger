package ru.nsu.fit.g15204.uzlov.messenger.kafka;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.InvalidTopicException;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Connection {
    private static String OFFSET_RESET = "earliest";
    private static Integer MAX_POLL_RECORDS = 100;

    private final ConcurrentLinkedQueue<TopicManagerImpl<?>> subscribeQueue = new ConcurrentLinkedQueue<>();

    private String brokersAddress;
    private String appId = "appId:" + UUID.randomUUID();


    public Connection(String brokersAddress) {
        this.brokersAddress = brokersAddress;

        final List<Reference<TopicManagerImpl<?>>> subscription = new ArrayList<>();
        ReferenceQueue<TopicManagerImpl<?>> referenceQueue = new ReferenceQueue<>();
        List<Reference<TopicManagerImpl<?>>> toRemove = new ArrayList<>();

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor(r -> {
            Thread t = Executors.defaultThreadFactory().newThread(r);
            t.setDaemon(true);
            t.setUncaughtExceptionHandler((t1, e) -> e.printStackTrace());
            return t;
        });
        executorService.scheduleWithFixedDelay(() -> {
            try {
                while (true) {
                    var tm = this.subscribeQueue.poll();
                    if (tm == null) {
                        break;
                    }
                    subscription.add(new WeakReference<>(tm, referenceQueue));
                }

                while (true) {
                    var ref = referenceQueue.poll();
                    if (ref == null) {
                        break;
                    }

                    // subscription is a list of Reference<TopicManagerImpl<?>>,
                    // but ref is Reference<?>
                    // so there is a warning
                    subscription.remove(ref);
                }

                for (var ref : subscription) {
                    var i = ref.get();
                    if (i == null) {
                        toRemove.add(ref);
                        continue;
                    }
                    i.update();
                }

                subscription.removeAll(toRemove);
                toRemove.clear();
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }, 0, 100, TimeUnit.MILLISECONDS);
    }

    private void addSubscription(TopicManagerImpl<?> topicManager) {
        this.subscribeQueue.add(topicManager);
    }

    private <T> Consumer<String, T> createConsumer(Class<? extends Serializer<T>> serializerClass) {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, this.brokersAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, this.appId + "%" + UUID.randomUUID());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, serializerClass.getName());
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, Connection.MAX_POLL_RECORDS);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, Connection.OFFSET_RESET);
        return new KafkaConsumer<>(props);
    }

    private <T> Producer<String, T> createProducer(Class<? extends Deserializer<T>> serializerClass) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, this.brokersAddress);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, this.appId);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, serializerClass.getName());
        //props.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, CustomPartitioner.class.getName());

        return new KafkaProducer<>(props);
    }

    public <T, Q extends Serializer<T> & Deserializer<T>> TopicManager<T> getTopicManager(Class<Q> serializerClass, String topic, Runnable callback) {
        var consumer = this.createConsumer(serializerClass);
        var producer = this.createProducer(serializerClass);

        return new TopicManagerImpl<>(consumer, producer, topic, callback, callback != null);
    }

    private class TopicManagerImpl<T> implements TopicManager<T> {
        private final Consumer<String, T> consumer;
        private final Producer<String, T> producer;
        private final String topic;
        private final Runnable callback;
        private final ConcurrentLinkedQueue<ConsumerRecords<String, T>> newRecords = new ConcurrentLinkedQueue<>();
        private final boolean async;

        private TopicManagerImpl(Consumer<String, T> consumer, Producer<String, T> producer, String topic, Runnable callback, boolean async) {
            this.consumer = consumer;
            this.producer = producer;
            this.topic = topic;
            this.async = async;

            this.consumer.subscribe(Collections.singletonList(topic));
            this.update(0);

            this.callback = callback;

            if (async) {
                Connection.this.addSubscription(this);
            }
        }

        private void update() {
            this.update(0);
        }

        private void update(long millis) {
            try {
                var record = this.consumer.poll(Duration.ofMillis(millis));

                if (!record.isEmpty()) {
                    this.newRecords.add(record);

                    if (this.async) {
                        if (this.callback != null) {
                            this.callback.run();
                        }
                    }
                }
            } catch (InvalidTopicException e) {
            }
        }

        @Override
        public ConsumerRecords<String, T> nextBatch() {
            return this.newRecords.poll();
        }

        @Override
        public void poll(long millis) {
            this.update(millis);
        }

        @Override
        public void send(T t) {
            this.producer.send(new ProducerRecord<>(this.topic, t));
        }

        @Override
        public void reset() {
            this.consumer.seekToBeginning(null);
        }

        public String getTopicName() {
            return this.topic;
        }
    }
}
