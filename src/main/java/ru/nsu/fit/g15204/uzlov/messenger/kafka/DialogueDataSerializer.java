package ru.nsu.fit.g15204.uzlov.messenger.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Scanner;

public class DialogueDataSerializer implements Serializer<DialogueData>, Deserializer<DialogueData> {
    @Override
    public DialogueData deserialize(String s, byte[] bytes) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8);
        String name = scanner.nextLine();
        return new DialogueData(name);
    }

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, DialogueData dialogue) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream, false, StandardCharsets.UTF_8);
        writer.println(dialogue.getName());
        writer.flush();
        return stream.toByteArray();
    }

    @Override
    public void close() {

    }
}
