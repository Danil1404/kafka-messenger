package ru.nsu.fit.g15204.uzlov.messenger;

public interface Message {
    String getAuthorName();

    String getContent();

    boolean isFromSelf();
}
