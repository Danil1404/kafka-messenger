package ru.nsu.fit.g15204.uzlov.messenger.gui;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.TextArea;
import javafx.scene.control.skin.ListViewSkin;
import javafx.scene.control.skin.VirtualFlow;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import ru.nsu.fit.g15204.uzlov.messenger.Dialogue;
import ru.nsu.fit.g15204.uzlov.messenger.Message;
import ru.nsu.fit.g15204.uzlov.messenger.SettingsProvider;

import java.util.ArrayList;

public class MessagesView {
    private final BorderPane mainPane = new BorderPane();
    private final Dialogue dialogue;
    private final Runnable returnCallback;
    private final TextArea textArea = new TextArea();
    private final SettingsProvider settingsProvider;
    private final ListView<MyMessagePane> listView;

    public MessagesView(Dialogue dialogue, Runnable returnCallback, SettingsProvider settingsProvider) {
        this.dialogue = dialogue;
        this.returnCallback = returnCallback;
        this.settingsProvider = settingsProvider;

        var list = this.dialogue.getMessages();

        this.listView = new ListView<>();
        ObservableList<MyMessagePane> observableList = FXCollections.observableList(new ArrayList<>());
        for (var m : list) {
            observableList.add(new MyMessagePane(m, this.listView.widthProperty()));
        }
        this.listView.setItems(observableList);
        this.listView.setSelectionModel(new NoSelectionModel<>());
        this.listView.setFocusTraversable(false);
        this.listView.scrollTo(this.listView.getItems().size() - 1);

        this.mainPane.setCenter(this.listView);

        this.dialogue.subscribeToUpdates(messages -> Platform.runLater(() -> {
            if (messages.isEmpty()) {
                return;
            }
            for (var m : messages) {
                observableList.add(new MyMessagePane(m, this.listView.widthProperty()));
            }
            this.updateScroll();
        }));

        Button returnButton = new Button("return");
        returnButton.setOnAction(event -> this.returnCallback.run());
        HBox topHBox = new HBox(returnButton);
        topHBox.setAlignment(Pos.TOP_LEFT);
        this.mainPane.setTop(topHBox);

        BorderPane messageCreationBox = new BorderPane();

        this.textArea.setWrapText(true);
        this.textArea.setPrefHeight(150.0);
        this.textArea.setMaxHeight(550.0);
        messageCreationBox.setCenter(this.textArea);

        Button sendButton = new Button("Send");
        HBox hBox = new HBox(sendButton);
        hBox.setAlignment(Pos.CENTER);
        messageCreationBox.setRight(hBox);
        sendButton.setMinWidth(100);
        sendButton.setMinHeight(100);
        sendButton.setOnAction(event -> this.sendMessageHandler());

        this.mainPane.setBottom(messageCreationBox);

        this.mainPane.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ESCAPE) {
                returnCallback.run();
                return;
            }
            this.textArea.requestFocus();
        });


        this.textArea.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER && keyEvent.isControlDown()) {
                this.sendMessageHandler();
            }
        });


        Platform.runLater(this.mainPane::requestFocus);
    }

    private void updateScroll() {
        try {
            ListViewSkin<?> ts = (ListViewSkin<?>) this.listView.getSkin();
            VirtualFlow<?> vf = (VirtualFlow<?>) ts.getChildren().get(0);

            var lastVisible = vf.getLastVisibleCell().getIndex();
            var lastIndex = this.listView.getItems().size() - 1;

            if (lastVisible >= lastIndex) {
                this.listView.scrollTo(lastIndex);
            }
        } catch (Exception ignored) {
        }
    }

    private boolean sendMessageHandler() {
        String text = this.textArea.getText();
        if (text.isEmpty()) {
            return false;
        }
        this.dialogue.createMessage(text);
        this.textArea.setText("");

        return true;
    }

    public Parent getContent() {
        return this.mainPane;
    }

    public void updateSettings() {
        for (var v : this.listView.getItems()) {
            v.updateSettings();
        }
    }

    private class MyMessagePane extends VBox {
        private final Message message;
        private Label author = new Label();
        private Label content = new Label();

        MyMessagePane(Message message, ReadOnlyDoubleProperty widthProperty) {
            this.message = message;

            this.getChildren().addAll(this.author, this.content);

            this.maxWidthProperty().bind(widthProperty.subtract(30.0));

            this.author.setText(message.getAuthorName());
            this.author.setWrapText(true);

            this.content.setText(message.getContent());
            this.content.setWrapText(true);

            this.updateSettings();

            if (message.isFromSelf()) {
                this.author.setContentDisplay(ContentDisplay.RIGHT);
                this.content.setContentDisplay(ContentDisplay.RIGHT);

                this.author.setAlignment(Pos.TOP_RIGHT);
                this.content.setAlignment(Pos.TOP_RIGHT);

                this.author.setTextAlignment(TextAlignment.RIGHT);
                this.content.setTextAlignment(TextAlignment.RIGHT);

                this.setAlignment(Pos.TOP_RIGHT);
            } else {
                this.author.setContentDisplay(ContentDisplay.LEFT);
                this.content.setContentDisplay(ContentDisplay.LEFT);

                this.author.setAlignment(Pos.TOP_LEFT);
                this.content.setAlignment(Pos.TOP_LEFT);

                this.author.setTextAlignment(TextAlignment.LEFT);
                this.content.setTextAlignment(TextAlignment.LEFT);

                this.setAlignment(Pos.TOP_LEFT);
            }

            this.requestLayout();
            this.content.requestLayout();
            this.author.requestLayout();
        }

        public Message getMessage() {
            return this.message;
        }

        public void updateSettings() {
            var fontSizeString = "-fx-font: " + MessagesView.this.settingsProvider.getTextSize() + " arial;";
            var headerStyleString = fontSizeString + "-fx-font-weight: bold;";
            this.author.setStyle(headerStyleString);
            this.author.setTextFill(Color.GRAY.desaturate());
            this.content.setStyle(fontSizeString);
        }
    }

    private class NoSelectionModel<T> extends MultipleSelectionModel<T> {

        @Override
        public ObservableList<Integer> getSelectedIndices() {
            return FXCollections.emptyObservableList();
        }

        @Override
        public ObservableList<T> getSelectedItems() {
            return FXCollections.emptyObservableList();
        }

        @Override
        public void selectIndices(int index, int... indices) {
        }

        @Override
        public void selectAll() {
        }

        @Override
        public void selectFirst() {
        }

        @Override
        public void selectLast() {
        }

        @Override
        public void clearAndSelect(int index) {
        }

        @Override
        public void select(int index) {
        }

        @Override
        public void select(T obj) {
        }

        @Override
        public void clearSelection(int index) {
        }

        @Override
        public void clearSelection() {
        }

        @Override
        public boolean isSelected(int index) {
            return false;
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public void selectPrevious() {
        }

        @Override
        public void selectNext() {
        }
    }
}
