package ru.nsu.fit.g15204.uzlov.messenger.gui;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import ru.nsu.fit.g15204.uzlov.messenger.AppManager;
import ru.nsu.fit.g15204.uzlov.messenger.Dialogue;
import ru.nsu.fit.g15204.uzlov.messenger.DialogueListManager;
import ru.nsu.fit.g15204.uzlov.messenger.SettingsProvider;

import java.util.ArrayList;
import java.util.List;

public class DialoguesView {
    private final BorderPane content = new BorderPane();
    private final BorderPane mainPane = new BorderPane();
    private final AppManager appManager;
    private final Stage stage;
    private final ListView<MyDialoguePane> listView = new ListView<>();
    private final SettingsProvider settingsProvider;
    private MessagesView messagesView = null;

    public DialoguesView(AppManager appManager, Stage stage, SettingsProvider settingsProvider) {
        this.appManager = appManager;
        this.stage = stage;
        this.settingsProvider = settingsProvider;

        this.listView.setOnMouseClicked(event -> this.openSelectedDialogue());


        this.listView.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                MyDialoguePane clicked = this.listView.getSelectionModel().getSelectedItem();
                if (clicked == null) {
                    return;
                }

                this.openSelectedDialogue();
            }
        });


        this.mainPane.setCenter(this.listView);

        HBox hBox = new HBox();
        Button refreshButton = new Button("Refresh");
        refreshButton.setOnAction(event -> this.initDialoguesList());
        Button newDialogueButton = new Button("Create");
        newDialogueButton.setOnAction(event -> this.createDialogueAction());
        hBox.getChildren().add(refreshButton);
        hBox.getChildren().add(newDialogueButton);
        hBox.setAlignment(Pos.TOP_RIGHT);
        this.mainPane.setTop(hBox);

        this.content.setCenter(this.mainPane);

        this.initDialoguesList();

        Platform.runLater(this.mainPane::requestFocus);
    }

    private void initDialoguesList() {
        var dialoguesManager = this.appManager.getDialogueListManager();
        var list = dialoguesManager.getDialogues();

        ObservableList<MyDialoguePane> observableList = FXCollections.observableList(new ArrayList<>());
        this.addDialoguesToList(list, observableList);
        this.listView.setItems(observableList);


        dialoguesManager.subscribeToUpdates(dialogues -> Platform.runLater(() -> this.addDialoguesToList(dialogues, observableList)));
    }

    private void openSelectedDialogue() {
        MyDialoguePane clicked = this.listView.getSelectionModel().getSelectedItem();
        if (clicked == null) {
            return;
        }
        this.messagesView = new MessagesView(clicked.getDialogue(), () -> {
            this.content.setCenter(this.mainPane);
            this.updateLastMessages();
            this.messagesView = null;
        }, this.settingsProvider);

        this.content.setCenter(this.messagesView.getContent());
    }

    private void updateLastMessages() {
        this.listView.getItems().forEach(myDialoguePane -> {
            var currentMessages = myDialoguePane.getDialogue().getMessages();
            if (currentMessages.size() > 0) {
                myDialoguePane.setLastMessage(currentMessages.get(currentMessages.size() - 1).getContent());
            }
            myDialoguePane.getDialogue().subscribeToUpdates(messages -> Platform.runLater(() -> myDialoguePane.setLastMessage(messages.get(messages.size() - 1).getContent())));
        });
    }

    private void addDialoguesToList(List<Dialogue> list, ObservableList<MyDialoguePane> observableList) {
        for (var d : list) {
            var p = new MyDialoguePane(d);
            var currentMessages = d.getMessages();
            if (currentMessages.size() > 0) {
                p.setLastMessage(currentMessages.get(currentMessages.size() - 1).getContent());
            }
            observableList.add(p);
            d.subscribeToUpdates(messages -> Platform.runLater(() -> p.setLastMessage(messages.get(messages.size() - 1).getContent())));
        }
    }

    public Parent getContent() {
        return this.content;
    }

    private void createDialogueAction() {
        TextField dialogueName = new TextField();

        Alert dialog = new Alert(Alert.AlertType.NONE, null, ButtonType.OK, ButtonType.CANCEL);
        dialog.setTitle("Create new dialogue");
        dialog.getDialogPane().setContent(dialogueName);
        dialog.initOwner(this.stage);
        dialog.showAndWait();

        ButtonType result = dialog.getResult();
        if (result == ButtonType.CANCEL) {
            return;
        }
        String name = dialogueName.getText();
        if (name.isEmpty()) {
            return;
        }

        try {
            this.appManager.getDialogueListManager().createDialogue(name);
        } catch (DialogueListManager.DialogueCreationException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Can't create dialogue", ButtonType.OK);
            alert.initOwner(this.stage);
            alert.showAndWait();
        }
    }

    public void updateSettings() {
        for (var v : this.listView.getItems()) {
            v.updateSettings();
        }
        if (this.messagesView != null) {
            this.messagesView.updateSettings();
        }
    }

    private class MyDialoguePane extends VBox {
        private final Dialogue dialogue;
        private Label name = new Label();
        private Label lastMessage = new Label();

        MyDialoguePane(Dialogue dialogue) {
            this.dialogue = dialogue;

            this.name.setText(dialogue.getName());
            this.name.setMaxWidth(Double.MAX_VALUE);

            this.lastMessage.setText(dialogue.getLastMessage());

            this.getChildren().addAll(this.name, this.lastMessage);

            this.updateSettings();
        }

        public Dialogue getDialogue() {
            return this.dialogue;
        }

        private void setLastMessage(String message) {
            this.lastMessage.setText(message);
        }

        public void updateSettings() {
            var fontSizeString = "-fx-font: " + DialoguesView.this.settingsProvider.getTextSize() + " arial;";
            var headerStyleString = fontSizeString + "-fx-font-weight: bold;";
            this.name.setStyle(headerStyleString);
            this.name.setTextFill(Color.GRAY.desaturate());
            this.lastMessage.setStyle(fontSizeString);
        }
    }
}
