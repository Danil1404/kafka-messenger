package ru.nsu.fit.g15204.uzlov.messenger.gui;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import ru.nsu.fit.g15204.uzlov.messenger.Credentials;

import java.util.function.Consumer;

public class RegistrationView {
    private final BorderPane wrapperPanel = new BorderPane();
    private final GridPane mainPanel = new GridPane();
    private final TextField nameField = new TextField();
    private final TextField passwordField = new TextField();
    private final Consumer<Credentials> registrationCallback;

    public RegistrationView(Consumer<Credentials> registrationCallback, Runnable cancelCallback) {
        this.registrationCallback = registrationCallback;

        this.wrapperPanel.setCenter(mainPanel);

        Button cancelButton = new Button("Return");
        cancelButton.setOnAction(event -> cancelCallback.run());
        this.wrapperPanel.setTop(cancelButton);

        this.nameField.setPromptText("your name");
        this.nameField.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER)  {
                this.handleClick();
            }
        });

        this.passwordField.setPromptText("password");
        this.passwordField.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                this.handleClick();
            }
        });

        Button button = new Button();
        button.setText("Sign up");
        button.setOnAction(event -> this.handleClick());

        VBox vBox = new VBox();
        vBox.getChildren().add(this.nameField);
        vBox.getChildren().add(this.passwordField);
        vBox.getChildren().add(button);
        vBox.setAlignment(Pos.CENTER);

        for (int i = 0; i < 3; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(100.0 / 3);
            this.mainPanel.getColumnConstraints().add(colConst);
        }
        for (int i = 0; i < 3; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(100.0 / 3);
            this.mainPanel.getRowConstraints().add(rowConst);
        }

        this.mainPanel.add(vBox, 1, 1);

        Platform.runLater(this.mainPanel::requestFocus);
    }

    private void handleClick() {
        var name = this.nameField.getText();
        if (name.isEmpty()) {
            return;
        }
        this.registrationCallback.accept(new Credentials(name, this.passwordField.getText()));
    }

    public Parent getContent() {
        return this.wrapperPanel;
    }
}
