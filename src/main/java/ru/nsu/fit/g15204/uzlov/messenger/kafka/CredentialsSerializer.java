package ru.nsu.fit.g15204.uzlov.messenger.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;
import ru.nsu.fit.g15204.uzlov.messenger.Credentials;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Scanner;

public class CredentialsSerializer implements Serializer<Credentials>, Deserializer<Credentials> {
    @Override
    public Credentials deserialize(String s, byte[] bytes) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8);
        String name = scanner.nextLine();
        String pass = scanner.hasNext() ? scanner.nextLine() : "";
        return new Credentials(name, pass);
    }

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, Credentials credentials) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream, false, StandardCharsets.UTF_8);
        writer.println(credentials.getName());
        writer.println(credentials.getPassword());
        writer.flush();
        return stream.toByteArray();
    }

    @Override
    public void close() {

    }
}
