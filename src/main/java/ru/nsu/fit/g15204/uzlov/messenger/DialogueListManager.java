package ru.nsu.fit.g15204.uzlov.messenger;

import java.util.List;
import java.util.function.Consumer;

public interface DialogueListManager {
    List<Dialogue> getDialogues();

    void createDialogue(String name) throws DialogueCreationException;

    void subscribeToUpdates(Consumer<List<Dialogue>> callback);

    class DialogueCreationException extends Exception {
        public DialogueCreationException() {
        }

        public DialogueCreationException(String message) {
            super(message);
        }

        public DialogueCreationException(String message, Throwable cause) {
            super(message, cause);
        }

        public DialogueCreationException(Throwable cause) {
            super(cause);
        }
    }
}
