package ru.nsu.fit.g15204.uzlov.messenger;

import java.util.List;
import java.util.function.Consumer;

public interface Dialogue {
    String getName();

    String getLastMessage();

    List<Message> getMessages();

    void createMessage(String content);

    void subscribeToUpdates(Consumer<List<Message>> callback);
}
