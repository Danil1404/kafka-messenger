package ru.nsu.fit.g15204.uzlov.messenger;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ru.nsu.fit.g15204.uzlov.messenger.gui.DialoguesView;
import ru.nsu.fit.g15204.uzlov.messenger.gui.LoginView;
import ru.nsu.fit.g15204.uzlov.messenger.gui.RegistrationView;
import ru.nsu.fit.g15204.uzlov.messenger.kafka.Connection;

import java.util.HashMap;

public class Main extends Application {
    private final BorderPane stagePane = new BorderPane();
    private final MenuBar menuBar = new MenuBar();
    private final HashMap<String, MenuItem> menus = new HashMap<>();
    private AppManager manager = null;
    private Stage primaryStage = null;
    private SettingsProviderImpl settingsProvider = new SettingsProviderImpl();
    private DialoguesView dialoguesView = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;

        primaryStage.setTitle("Kafka Messenger");
        primaryStage.setMinWidth(700);
        primaryStage.setMinHeight(500);

        primaryStage.setOnCloseRequest(event -> {

        });

        var connection = new Connection("localhost:9092");
        this.manager = new AppManager(connection);

        this.stagePane.setTop(this.menuBar);
        this.fillMenubar();

        var view = new LoginView(this::handleLogin, this::openRegisterPage);
        this.stagePane.setCenter(view.getContent());

        primaryStage.setScene(new Scene(this.stagePane, 1000, 700));
        primaryStage.show();
    }

    private void fillMenubar() {
        var file = new Menu("File");
        this.menuBar.getMenus().add(file);

        var sOUt = new MenuItem("Sign out");
        this.menus.put("sign out", sOUt);
        file.getItems().add(sOUt);
        sOUt.setOnAction(event -> {
            var view = new LoginView(this::handleLogin, this::openRegisterPage);
            this.stagePane.setCenter(view.getContent());
            this.dialoguesView = null;
        });

        var edit = new Menu("Edit");
        this.menuBar.getMenus().add(edit);

        var textSize = new MenuItem("Text size");
        this.menus.put("text size", textSize);
        edit.getItems().add(textSize);
        textSize.setOnAction(event -> {
            TextField sizeField = new TextField();

            Alert dialog = new Alert(Alert.AlertType.NONE, null, ButtonType.OK, ButtonType.CANCEL);
            dialog.setTitle("Change font size");
            dialog.getDialogPane().setContent(sizeField);
            dialog.initOwner(this.primaryStage);
            dialog.showAndWait();

            ButtonType result = dialog.getResult();
            if (result == ButtonType.CANCEL) {
                return;
            }
            String size = sizeField.getText();
            if (size.isEmpty()) {
                return;
            }
            try {
                double value = Double.parseDouble(size);
                if (value < 10 || value > 50) {
                    return;
                }
                this.settingsProvider.setTextSize(value);

                if (this.dialoguesView != null) {
                    this.dialoguesView.updateSettings();
                }
            } catch (NumberFormatException ignored) {
            }
        });
    }

    private void handleLogin(Credentials credentials) {

        Alert loginAlert = new Alert(Alert.AlertType.INFORMATION, "Trying to login...", ButtonType.OK);
        loginAlert.initOwner(this.primaryStage);
        loginAlert.show();
        this.manager.login(aBoolean -> {
            loginAlert.close();
            if (aBoolean) {
                var view = new DialoguesView(this.manager, this.primaryStage, this.settingsProvider);
                this.stagePane.setCenter(view.getContent());
                this.dialoguesView = view;
            } else {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR, "Can't login", ButtonType.OK);
                errorAlert.initOwner(this.primaryStage);
                errorAlert.showAndWait();
            }
        }, credentials);
    }

    private void openRegisterPage() {
        var view = new RegistrationView(credentials -> {
            var success = this.manager.register(credentials);

            if (!success) {
                Alert errorAlert = new Alert(Alert.AlertType.ERROR, "Can't register", ButtonType.OK);
                errorAlert.initOwner(this.primaryStage);
                errorAlert.showAndWait();
                return;
            }

            var loginView = new LoginView(this::handleLogin, this::openRegisterPage);
            this.stagePane.setCenter(loginView.getContent());
        }, () -> {
            var loginView = new LoginView(this::handleLogin, this::openRegisterPage);
            this.stagePane.setCenter(loginView.getContent());
        });
        this.stagePane.setCenter(view.getContent());
    }

    private static class SettingsProviderImpl implements SettingsProvider {
        private double size = 15;

        @Override
        public double getTextSize() {
            return this.size;
        }

        public void setTextSize(double size) {
            this.size = size;
        }
    }
}
