package ru.nsu.fit.g15204.uzlov.messenger.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecords;

public interface TopicManager<T> {
    ConsumerRecords<String, T> nextBatch();

    void poll(long millis);

    void send(T t);

    void reset();
}
