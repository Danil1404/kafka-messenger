package ru.nsu.fit.g15204.uzlov.messenger.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Scanner;

public class MessageDataSerializer implements Serializer<MessageData>, Deserializer<MessageData> {
    @Override
    public MessageData deserialize(String s, byte[] bytes) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8);
        String authorName = scanner.nextLine();
        if (scanner.hasNext()) {
            String content = scanner.nextLine().replace("\\n", "\n").replace("\\\\", "\\");
            return new MessageData(authorName, content);
        }
        return new MessageData("", authorName);
    }

    @Override
    public void configure(Map<String, ?> map, boolean b) {

    }

    @Override
    public byte[] serialize(String s, MessageData messageData) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(stream, false, StandardCharsets.UTF_8);
        writer.println(messageData.getAuthorName());
        writer.println(messageData.getContent().replace("\\", "\\\\").replace("\n", "\\n"));
        writer.flush();
        return stream.toByteArray();
    }

    @Override
    public void close() {

    }
}
