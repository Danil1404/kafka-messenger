package ru.nsu.fit.g15204.uzlov.messenger;

public interface SettingsProvider {
    double getTextSize();
}
