package ru.nsu.fit.g15204.uzlov.messenger;

public class Credentials {
    private final String name;
    private final String password;

    public Credentials(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }
}
